
classname = BridgeUrlParser
htmlfile = bridge-urls.html
bridgefile = bridge-rc
javaoutfile = java-out-rc
sources = $(classname).java
classes = $(sources:.java=.class)

all: $(classes) $(htmlfile) $(bridgefile)
	@echo java $(classname)
	@java $(classname) $(shell sed -n 's,.*href=\("[^"]*"\).*,\1,gp' $(htmlfile)) | sort -u > $(javaoutfile)
	diff -uw $(bridgefile) $(javaoutfile)

$(htmlfile): convert-bridge-to-uris.py
	./convert-bridge-to-uris.py README.md

$(bridgefile): README.md
	sed -n 's,^\(Bridge .*\)$$,\1,p' README.md | sort -u > $(bridgefile)

clean :
	rm -f $(classes) $(htmlfile) $(bridgefile) $(javaoutfile)

%.class: %.java
	javac $<
