function parseQuery(queryString) {
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}

var inviteLink = document.location.href;
var fragment = inviteLink.substring(inviteLink.indexOf("#") + 1);
query = parseQuery(fragment.substring(fragment.indexOf("?") + 1));
    
bridgeLink = "bridge://" + query["transport"] + "@" + query["ip"] + ":" + query["orport"] + "/";
delete query["transport"]
delete query["ip"]
delete query["orport"]
bridgeLink += fragment;

document.getElementById("bridgeLink").innerHTML=bridgeLink;

if (inviteLink.indexOf("#") > 0) {
    document.getElementById("buttonConnectToBridge").style.visibility="visible";
    document.getElementById("buttonConnectToBridge").onclick = function() {
        document.location.href=bridgeLink;
    };
}
