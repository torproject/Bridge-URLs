
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class BridgeUrlParser {

    public static void main (String[] args)
        throws java.net.URISyntaxException, UnsupportedEncodingException {
        for (String s: args) {
            URI uri = new URI(s.trim());
            StringBuilder builder = new StringBuilder("Bridge ");
            Map<String, String> query = splitQuery(uri);
            if ("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())) {
                String transport = query.get("transport");
                if (transport != null && transport.length() > 0) {
                    builder.append(transport);
                    builder.append(' ');
                }
                builder.append(query.get("ip"));
                builder.append(':');
                builder.append(query.get("orport"));
                query.remove("transport");
                query.remove("ip");
                query.remove("orport");
            } else if ("bridge".equals(uri.getScheme())) {
                String transport = uri.getUserInfo();
                if (transport != null && transport.length() > 0) {
                    builder.append(transport);
                    builder.append(' ');
                }
                builder.append(uri.getHost());
                builder.append(':');
                builder.append(uri.getPort());
            } else {
                throw new URISyntaxException(s, "Unsupported bridge URI scheme!");
            }
            builder.append(' ');
            builder.append(uri.getPath().substring(1));
            for (Entry<String, String> entry : query.entrySet()) {
                String key = entry.getKey();
                if (key != null) {
                    builder.append(' ');
                    builder.append(key);
                    String value = entry.getValue();
                    if (key.length() > 0 && value != null && value.length() > 0) {
                        builder.append('=');
                        builder.append(value);
                    }
                }
            }
            System.out.println(builder.toString().trim());
        }
    }

    public static Map<String, String> splitQuery(URI uri) throws UnsupportedEncodingException {
        final Map<String, String> query_pairs = new LinkedHashMap<>();
        final String[] pairs = uri.getQuery().split("&");
        for (String pair : pairs) {
            final int firstEquals = pair.indexOf("=");
            final String key = firstEquals > 0
                ? pair.substring(0, firstEquals)
                : pair;
            final String value = firstEquals > 0 && pair.length() > firstEquals + 1
                ? pair.substring(firstEquals + 1)
                : null;
            query_pairs.put(key, value);
        }
        return query_pairs;
    }
}
