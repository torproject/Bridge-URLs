#!/usr/bin/env python3

import collections
import re
import sys
import urllib.parse

if len(sys.argv) == 1:
    files = ('/etc/tor/torrc', )
else:
    files = sys.argv[1:]
print('Reading Bridge lines from:', ', '.join(files))

pattern = re.compile(r'^Bridge *([a-z0-9_]+)? +([0-9a-fA-F.:\[\]]{7,}):([0-9]{1,5}) +([0-9A-Fa-f]{40}) *(.*)$')

outputfp = open('bridge-urls.html', 'w')
outputfp.write('<html><body><h1>Bridge URLs</h1><ul>')

bridges = set()
for f in files:
    with open(f) as fp:
        for line in fp:
            m = pattern.match(line)
            if m:
                bridges.add(line)

for bridge in sorted(bridges):
    m = pattern.match(bridge)
    transport = m.group(1)
    ip = m.group(2)
    orport = m.group(3)
    fingerprint = m.group(4)
    querystring = collections.OrderedDict()
    for item in m.group(5).strip().split(' '):
        if not item:
            continue
        key, value = item.split('=')
        querystring[key] = value
    if transport:
        outputformat = '''
<li><pre>{bridge}</pre><a href="bridge://{transport}@{ip}:{orport}/{fingerprint}?{querystring}"><code>bridge:</code></a> &bull; 
<a href="http://bridge.onion/{fingerprint}?transport={transport}&ip={ip}&orport={orport}&{querystring}">
<code>http://bridge.onion</code></a> &bull; 
<a href="https://bridges.torproject.org/{fingerprint}?transport={transport}&ip={ip}&orport={orport}&{querystring}">
<code>https://bridges.torproject.org</code></a>
</li>'''
    else:
        outputformat = '''
<li><pre>{bridge}</pre><a href="bridge://{ip}:{orport}/{fingerprint}?{querystring}"><code>bridge:</code></a> &bull; 
<a href="http://bridge.onion/{fingerprint}?ip={ip}&orport={orport}&{querystring}">
<code>http://bridge.onion</code></a> &bull; 
<a href="https://bridges.torproject.org/{fingerprint}?transport=&ip={ip}&orport={orport}&{querystring}">
<code>https://bridges.torproject.org</code></a>
</li>'''
    output = outputformat.format(bridge=m.group(),
                                 transport=transport,
                                 ip=ip,
                                 orport=orport,
                                 fingerprint=fingerprint,
                                 querystring=urllib.parse.urlencode(querystring))
    outputfp.write(output)

outputfp.write('</ul></body></html>')
outputfp.close()
