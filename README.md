
# Bridge URLs

Setting up a Bridge in Tor requires entering some specific configuration entries into the right place.  Encoding the configuration in a standard URI format can ease a lot of that pain.

Follow implementation here:
https://trac.torproject.org/projects/tor/ticket/15035

## Test Bridges

These are some configuration lines that are in the standard _torrc_ format for adding bridges to tor daemon.  This are based on real bridges, but the numbers are mixed up a little, so they are not useful as real bridges.

```
Bridge 168.80.66.94:443 0BCF64A49F1F1414EA1CE6D7938D19E0198A41D5
Bridge 18.152.86.23:9001 B8B46245BA58DBA4FFEF1A47D9051E6BD450AE1B
Bridge 189.163.13.104:23750 F3CFDB00C5437EC4478529FEDEE194AC70BA87D5
Bridge meek_lite 0.0.2.0:2 97700DFE9F483596DDA6264C4D7DF7641E1E39CE url=https://meek.azureedge.net/ front=ajax.aspnetcdn.com
Bridge obfs4 104.224.78.19:443 FD9DAEE45A2FDF70D462914A75ADE99A29957920 cert=LSOd9qOffpIFM4az+ueou7sY0eQRAsI/joW4QgCl/LSDo2ecQzAQHNu281oAivLDZuTQNA iat-mode=0
Bridge obfs4 85.11.218.129:443 4C4441484D06047C1A12F768FC7E7268F2E6DD49 cert=FFKeJPokZXigyKpn+E/iKim/FwNEiIdifbHfaXQmyu1QpSHtNlruAIWebci9m8Yb0tQUOw iat-mode=0
Bridge obfs4 24.95.77.155:8080 05CF7049A8AC95A26E8F4611EF9D9625F0589990 cert=Fy8xOhAY1Rm+V4yqv//gYDF9duiYgegERNMFO1Rdk7h/ntM+QtZqmzjdu7o0rx9nz99OFg iat-mode=0
Bridge obfs4 94.206.82.75:9443 8991E2D5D9751E0AE9A5D2262F894FF98417AF41 cert=ZbzndskJ9+clCg5FcfRkF2zJMqNZDvLFPlJ9TYKKwZMq69DzDnZaqcMlle7udxTWrYR+Rg iat-mode=0
Bridge obfs4 72.17.139.46:40687 FF41456A5C061DF69E4654F4858199911860CF81 cert=sL0vJhsVKuHO0k7xhOpg/UUHZuftbAQPm0qKUrc9sQWyLWfSNeKI0RzhL+a9TpLMaIoURg iat-mode=0
Bridge obfs4 78.215.187.186:45675 AE907EE5FAA5D0D27E0C83EFA6ADF8E79FCC0FF1 cert=/TRjMo+RinKaixARMjMtZZBhystaBe+aDaapPrbiITFtWx3M/AJcvpjHjO54tJqLd1+IWQ iat-mode=0
Bridge obfs4 107.160.7.24:443 7A0904F6D182B81BEFE0DEDAFEC974494672627B cert=a5/IlZMnDvb8d92LTHMfsBIgL7QlDLPiXiLwe85uedC80mGD0QerygzmsWnMEdwG9ER9Eg iat-mode=0
Bridge obfs4 79.136.160.201:46501 66AC975BF7CB429D057AE07FC0312C57D61BAEC1 cert=dCtn9Ya8z+R8YQikdWgC3XTAt58z5Apnm95QHrJwnhFSdnphPPEz+NMm6OawWc2srKLjJg iat-mode=0
```

## Implementation Outline

Here's an outline of the possible URI formats for bridges:

```
Bridge 168.80.66.94:443 0BCF64A49F1F1414EA1CE6D7938D19E0198A41D5
bridge://168.80.66.94:443/0BCF64A49F1F1414EA1CE6D7938D19E0198A41D5
http://bridge.onion/168.80.66.94:443/0BCF64A49F1F1414EA1CE6D7938D19E0198A41D5
https://bridges.torproject.org/168.80.66.94:443/0BCF64A49F1F1414EA1CE6D7938D19E0198A41D5


Bridge meek_lite 0.0.2.0:2 97700DFE9F483596DDA6264C4D7DF7641E1E39CE url=https://meek.azureedge.net/ front=ajax.aspnetcdn.com
bridge://meek_lite@0.0.2.0:2/97700DFE9F483596DDA6264C4D7DF7641E1E39CE?url=https%3A//meek.azureedge.net/&front=ajax.aspnetcdn.com
http://bridge.onion/97700DFE9F483596DDA6264C4D7DF7641E1E39CE?transport=meek_lite&ip=0.0.2.0&port=2&url=https%3A//meek.azureedge.net/&front=ajax.aspnetcdn.com
https://bridges.torproject.org/97700DFE9F483596DDA6264C4D7DF7641E1E39CE?transport=meek_lite&ip=0.0.2.0&port=2&url=https%3A//meek.azureedge.net/&front=ajax.aspnetcdn.com


Bridge obfs4 104.224.78.19:443 FD9DAEE45A2FDF70D462914A75ADE99A29957920 cert=LSOd9qOffpIFM4az+ueou7sY0eQRAsI/joW4QgCl/LSDo2ecQzAQHNu281oAivLDZuTQNA iat-mode=0
bridge://obfs4@104.224.78.19:443/FD9DAEE45A2FDF70D462914A75ADE99A29957920?cert=LSOd9qOffpIFM4az%2Bueou7sY0eQRAsI%2FjoW4QgCl/LSDo2ecQzAQHNu281oAivLDZuTQNA&iat-mode=0
http://bridge.onion/FD9DAEE45A2FDF70D462914A75ADE99A29957920?transport=obfs4&ip=104.224.78.19&port=443&cert=LSOd9qOffpIFM4az%2Bueou7sY0eQRAsI%2FjoW4QgCl/LSDo2ecQzAQHNu281oAivLDZuTQNA&iat-mode=0
https://bridges.torproject.org/FD9DAEE45A2FDF70D462914A75ADE99A29957920?transport=obfs4&ip=104.224.78.19&port=443&cert=LSOd9qOffpIFM4az%2Bueou7sY0eQRAsI%2FjoW4QgCl/LSDo2ecQzAQHNu281oAivLDZuTQNA&iat-mode=0
```


## `bridge:` link for use in-app

The most popular custom scheme that I can think of are `magnet:` links for torrents.  They are a very similar idea as a bridge link.  So we should be able to use BitTorrent apps on all platfroms as an example of how to handle these links.  Using a custom URI scheme makes it easier to make a link that the browser will never handle, and always look for an app to send it too.  Also, using a custom URI Scheme like `bridge:` means that clicking these URIs in apps is much less likely to ever go to another app, since those apps would have to have specifically created the association.  Also, browser let you specific custom handlers for URI Schemes they do not understand.  These can be specified to closely match the `Bridge` lines in ''torrc''.  So this collection of fake bridges:

In desktop Firefox, you can add support for bridge: URIs by adding a custom "Protocol Handler":

1. load [about:config](about:config)
2. Right-click -> New -> Boolean -> Name: ''network.protocol-handler.expose.bridge'' -> Value -> ''false''
3. Next time you click a link of protocol-type ''bridge'' you will be asked which application to open it with. 

Chromium on Ubuntu will automatically prompt to open with `xdg-open`.  That will then use the standard, cross-distro XDG methods for registering and using custom schemes that are used in GNOME, KDE, etc. etc.  Tor Browser's ''.desktop'' file should be able to do this.  Any Debian/etc. package can install and configure the ''.desktop'' files and XDG Mime stuff as part of the package install.  Here's a manual example:

```console
$ cp bridge-url-parser.py /tmp/
$ chmod 0755 /tmp/bridge-url-parser.py
$ mkdir ~/.local/share/applications/
$ cp bridge-url-parser.desktop ~/.local/share/applications/
$ sudo update-desktop-database
$ xdg-mime default bridge-url-parser.desktop x-scheme-handler/bridge
```

There are similar mechanisms on Windows and MacOS.  For example MacOS has the `open` util which does was GNU/Linux's `xdg-open` does.


## `http:` link with fake domain name

One problem with having the bridge link clickable and having a real domain name is that it could leak info to the internet that the user is looking for a bridge.  So there could be a fake domain name for these links to provide a pattern to match for in things like the OS-level URL matchers in Android and iOS.  Something like `http://bridge.onion/` will never exist but is somehow a reserved domain name.  These links would then automatically show up as clickable on desktop and mobile apps, but should never cause network traffic.

his will lead to confusing failures since the browser will say "bridge.onion’s server IP address could not be found."



## `https:` link with real domain name, like ChatSecure

Using a real domain name like `https://bridges.torproject.org` provides a way to provide the user direct feedback when they are trying to add a bridge via URL.  If the user clicks on a bridge URL and it fails to go to Tor Browser, then they'll see the page on `bridges.torproject.org`.  If the config information is included in the query string, then `bridges.torproject.org` can automatically generate a proper `bridge:` URI, which the user can click to send the info out of the browser.  The page can also provide a HOWTO for setting up `bridge:` support for their OS.

ChatSecure's invite links have a nice feature in that Android and iOS will automatically route them to the ChatSecure app, even if they are clicked in the browser.  Then if the Zom app is not installed, the link will open up in the browser with instructions on how to get Zom and use the links.  With these links, the data is included in the URI Fragment, (e.g. stuff after `#`) which should never be sent to the network.  That protects privacy, but makes it harder for the server to provide useful feedback.  Here is an example URI:
```
https://chatsecure.org/i/#YWhkdmRqZW5kYmRicmVpQGR1a2dvLmNvbT9vdHI9M0EyN0FDODZBRkVGOENGMDlEOTAyMEQwNTJBNzNGMUVGMEQyOUI2Rg
```

Android has this method of registering a domain name as the official domain name that is tied to the app.  It has the nice feature that any link with that domain name will go straight to the matching app, and not be sent to other apps.  It does require some setup on the web server.  The big downside is that it only works if that app only claims links with that domain name and nothing else.  We tried using this in F-Droid, but did not want to give up claiming `market:` and `https://play.google.com` links.  They are known as "Android App Links".  iOS also has this, they are known as "Apple Universal Links".


** Use URL-safe base64!!**

Right now, the data is encoded in standard base64, which means it had to be encoded and decoded to work in URIs.  It would be better to switch to URL-safe base64 (RFC4648), which replaces the use of the chars **+** and **/** with **-** and **_**.  This is because and **_**.  **+** and **/** have special meaning in URIs.  I don't know what all this entails, so for now, the URL generation will need to know to URL-encode **+**, **/**, and **=**. (e.g. **%2B**, **%2F**, and **%3D**).  Just encoding **+** is often enough, since **/** often works fine in a URL query string value, and the base64 padding character **=** does not seem to be often used in bridge configs.
