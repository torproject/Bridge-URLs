#!/usr/bin/env python3
#
# https://www.torproject.org/docs/bridges
# https://www.torproject.org/docs/tor-manual-dev.html.en#Bridge
# Bridge [transport] IP:ORPort [fingerprint]

import os
import sys
from urllib.parse import urlparse, parse_qs, parse_qsl

os.chdir(os.path.dirname(__file__))

with open('bridge-url-parser.log', 'a') as fp:
    fp.write('---------------------------------------\n%s\n' % ' '.join(sys.argv))
    for arg in sys.argv[1:]:
        url = urlparse(arg.strip())
        with open('torrc', 'a') as fp:
            fp.write('Bridge ')
            if url.scheme == 'bridge':
                if url.username:  # transport is optional
                    fp.write(url.username)
                    fp.write(' ')
                fp.write(url.path.strip('/'))  # strip / added as part of URL creation
                for k, v in parse_qsl(url.query):
                    fp.write(' ')
                    fp.write(k)
                    fp.write('=')
                    fp.write(v)
            elif url.hostname == 'bridge.onion' or url.hostname == 'bridges.torproject.org':
                qsd = parse_qs(url.query)
                transport = qsd.get('transport')
                if transport:
                    fp.write(qsd['transport'])
                    fp.write(' ')
                fp.write(qsd['ip'])
                fp.write(':')
                fp.write(qsd['orport'])
                fp.write(' ')
                fp.write(url.path.strip('/'))  # strip / added as part of URL creation
                del(qsd['transport'])
                del(qsd['ip'])
                del(qsd['orport'])
                for k, v in qsd.items():
                    fp.write(' ')
                    fp.write(k)
                    fp.write('=')
                    fp.write(v)
            fp.write('\n')
                
